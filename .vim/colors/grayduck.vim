set bg=dark
hi clear

" Load the syntax highlighting defaults, if it's enabled.
if exists("syntax_on")
    syntax reset
endif

let colors_name = "grayduck"

hi Normal           ctermfg=252 ctermbg=233 guifg=#d0d0d0 guibg=#121212
" hi Normal           ctermfg=252 ctermbg=235 guifg=#d0d0d0 guibg=#262626

hi cursorline       cterm=none ctermbg=32
set nocursorline
hi LineNr           ctermfg=244 ctermbg=234
hi CursorLineNr     ctermfg=14 ctermbg=234

" term=bold gui=bold
hi Comment          ctermfg=245 guifg=#8a8a8a
hi Constant         ctermfg=252 guifg=#d0d0d0
hi Special          ctermfg=252 guifg=#d0d0d0
hi Identifier       ctermfg=252 guifg=#d0d0d0
hi Statement        ctermfg=245 guifg=#a8a8a8
hi PreProc          ctermfg=246 guifg=#949494
hi Type             ctermfg=252 guifg=#d0d0d0
hi TypeDef          ctermfg=252 guifg=#d0d0d0
hi PreProc          ctermfg=252 guifg=#d0d0d0
hi Conditional      ctermfg=252 guifg=#d0d0d0
hi Underlined       ctermfg=252 guifg=#d0d0d0
hi Error            ctermfg=252 ctermbg=124 guifg=#8a8a8a guibg=#af0000

hi Include          ctermfg=246 guifg=#949494 cterm=bold gui=bold
hi String           ctermfg=246 guifg=#949494

hi groovyELExpr     ctermfg=252 guifg=#d0d0d0

hi SpellBad         ctermbg=240

hi Visual           ctermfg=252 ctermbg=27
"hi Visual           ctermfg=252 ctermbg=236
