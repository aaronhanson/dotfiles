" Load vim-plug
if empty(glob("~/.vim/autoload/plug.vim"))
    execute '!curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.github.com/junegunn/vim-plug/master/plug.vim'
endif

call plug#begin()
"Plug 'kien/ctrlp.vim'
"Plug 'majutsushi/tagbar'
"Plug 'aaronhanson/presenting.vim', { 'branch': 'custom-cursor-location' }
"Plug 'mileszs/ack.vim'
"Plug 'maralla/completor.vim'
"Plug 'davidhalter/jedi-vim'
Plug 'editorconfig/editorconfig-vim'
call plug#end()

"let g:completor_python_binary='/usr/lib/python3/dist-packages/jedi'
"let g:completor_auto_trigger=0

" support ag
"let g:ackprg = 'ag --nogroup --nocolor --column'

let g:ctrlp_max_files=50000

filetype plugin on
syntax on

" put the cursor at the bottom of presentations
au FileType text let b:presenting_slide_separator = '\v(^|\n)\~{3,}'
let g:presenting_cursor_location="gg 0"

" Tweaks for browsing
set path+=**
set wildmenu
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=0  " use the same window
let g:netrw_liststyle=3     " tree view

" Locations to look for ctags
set tags=.tags

set encoding=utf-8
set visualbell

set nopaste " paste resets a bunch of stuff :(

set autoindent
set expandtab
set smarttab
set shiftwidth=4
set softtabstop=4
set tabstop=4           " number of spaces for tab

set title               " show title in console title bar
set backspace=indent,eol,start
set cursorline
set showcmd
set numberwidth=5
set laststatus=2
set ruler
set number
set relativenumber

set wildignore+=*.class,.git,.hg,.svn,**/build/**,**/target/test-reports/**,**/target/classes/**,**/target/work/**,*.pyc,*.swp,**/node_modules/**

" show special whitespace
set list
let &listchars="tab:\u203A\u203A,trail:\u2027"

" more natural buffer splitting
set splitbelow
set splitright

set undofile
set undodir=~/.vim/tmp,/var/tmp,/tmp
set backupdir=~/.vim/tmp,/var/tmp,/tmp
set directory=~/.vim/tmp,/var/tmp,/tmp

" get rid of cpp keyword highlight in groovy scripts
let groovy_allow_cpp_keywords=1
let groovy_highlight_functions=1

"
" custom mappings
"

nnoremap ; :

inoremap <special> jk <ESC>

" set , as the leader key instead of \
let mapleader=","

" close and remove the current buffer
nnoremap <Leader>c :bd<CR>
nnoremap <Leader>, :bp<CR>
nnoremap <Leader>. :bn<CR>

" show and hide line numberl
nnoremap <Leader>l :setlocal number!<CR>
nnoremap <Leader>rl :setlocal relativenumber!<CR>
nnoremap <Leader>nl :setlocal nonumber <BAR> :setlocal norelativenumber<CR>

" set the line number column colors
highlight LineNr ctermfg=grey ctermbg=240

" show and hide various line columns
highlight ColorColumn ctermbg=8
nnoremap <Leader>c0 :set colorcolumn=0<CR>
nnoremap <Leader>c8 :set colorcolumn=81<CR>
nnoremap <Leader>c10 :set colorcolumn=101<CR>

" ctrlp mapping
"nnoremap <Leader>p <C-p>
nnoremap <Leader>p :set paste!<CR>

" git gutter
nnoremap <Leader>ggt :GitGutterToggle<CR>

" various block comments
vnoremap <Leader># :s/^\(#\)\?/\=submatch(0) == "" ? "#" : ""/<ESC>
vnoremap <Leader>/ :s/^\(\/\/\)\?/\=submatch(0) == "" ? "\/\/" : ""/<ESC>
vnoremap <Leader>' <S-i>'<ESC>
vnoremap <Leader>" <S-i>"<ESC>

" screen navigation
"nmap <Leader>f <C-f>
"nmap <Leader>b <C-b>
"nmap <Leader>u <C-u>
"nmap <Leader>d <C-d>

" move between splits
nnoremap <Leader>wj <C-w>j
nnoremap <Leader>wk <C-w>k
nnoremap <Leader>wh <C-w>h
nnoremap <Leader>wl <C-w>l

" resize splits (focus, height, width, normalize)
nnoremap <Leader>wf <C-w>\| <C-w>_
nnoremap <Leader>w_ <C-w>_
nnoremap <Leader>w\| <C-w>\|
nnoremap <Leader>w= <C-w>=
nnoremap <Leader>ws :resize<Space>
nnoremap <Leader>wvs :vertical resize<Space>

" close current split
nnoremap <Leader>wq <C-w>q

" close all other splits
nnoremap <Leader>wo <C-w>o

" block indenting, keeping selection
vnoremap < <gv
vnoremap > >gv
vnoremap = =gv

set foldmethod=syntax
set foldlevelstart=20

" make sure color schemes work in tmux
if &term =~ '256color'
    " disable Background Color Erase (BCE) so that color schemes
    " render properly when inside 256-color tmux and GNU screen.
    " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
    set t_ut=
endif


" figure out what syntax group something is in
nnoremap <Leader>sg :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
            \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
            \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc
nnoremap <Leader>ss :call <SID>SynStack()<CR>

" Python test aliases
function! RunPython()
    silent !clear
    let f=expand("%")
    echomsg 'running python script ' . f
    execute '!python ' . f
endfunction

function! RunPython2()
    silent !clear
    let f=expand("%")
    echomsg 'running python script ' . f
    execute '!python2 ' . f
endfunction

function! RunPython3()
    silent !clear
    let f=expand("%")
    echomsg 'running python script ' . f
    execute '!python3 ' . f
endfunction

function! RunPythonTest()
    let cursor_pos=getpos('.')
    let f=substitute(substitute(expand("%:r"), '\./', '', 'g'), '/', '.', 'g')
    let curLine=getline('.')
    if !match(curLine, 'class')
      echomsg curLine
      normal 0w
      let testClass=expand("<cword>")
      silent !clear
      execute '!python -m unittest -v ' . f . '.' . testClass
    else
      let testMethod=expand("<cword>")
      normal [[w
      let testClass=expand("<cword>")
      call setpos('.', cursor_pos)
      silent !clear
      "echomsg 'running python test script ' . f . '.' . testClass . '.' . testMethod
      execute '!python -m unittest -v ' . f . '.' . testClass . '.' . testMethod
    endif
endfunction

function! RunPython3Test()
    let cursor_pos=getpos('.')
    let f=substitute(substitute(expand("%:r"), '\./', '', 'g'), '/', '.', 'g')
    let curLine=getline('.')
    if !match(curLine, 'class')
      echomsg curLine
      normal 0w
      let testClass=expand("<cword>")
      silent !clear
      execute '!python -m unittest -v ' . f . '.' . testClass
    else
      let testMethod=expand("<cword>")
      normal [[w
      let testClass=expand("<cword>")
      call setpos('.', cursor_pos)
      silent !clear
      execute '!python3 -m unittest -v ' . f . '.' . testClass . '.' . testMethod
    endif
endfunction

nnoremap <leader>rp :call RunPython()<CR>
nnoremap <leader>rp2 :call RunPython2()<CR>
nnoremap <leader>rp3 :call RunPython3()<CR>
nnoremap <leader>rt :call RunPythonTest()<CR>
nnoremap <leader>rt3 :call RunPython3Test()<CR>

"autocmd! BufNewFile,BufRead *.go setlocal ft=go
au! BufNewFile,BufRead *.go setlocal ft=go
"au! BufNewFile,BufRead *.txt setf txt

"au! BufNewFile,BufRead *.py colorscheme hans-gray
"    \ set tabstop=4
"    \ set softtabstop=4
"    \ set shiftwidth=4
"    \ set textwidth=79
"    \ set expandtab
"    \ set autoindent
"    \ set fileformat=unix

au! BufNewFile,BufRead *.sql setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab autoindent

color grayduck
