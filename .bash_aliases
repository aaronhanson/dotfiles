alias ltree="tree -L 2"
alias vi="vim"
alias tmux="tmux -2"
alias ls="ls --color=auto"
